package at.ac.tuwien.dsg.smartcom.adapter.util;

import at.ac.tuwien.dsg.smartcom.adapter.PushTask;

import java.util.concurrent.Future;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
public interface TaskScheduler {

    /**
     * Schedule a task
     *
     * @param task the task that should be scheduled
     */
    public Future schedule(PushTask task);
}
