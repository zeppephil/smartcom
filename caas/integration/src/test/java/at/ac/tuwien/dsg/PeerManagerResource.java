package at.ac.tuwien.dsg;

import at.ac.tuwien.dsg.smartcom.model.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PeerManagerResource {

    @GET
    @Path("/collectiveInfo/{id}")
    public CollectiveInfo getCollectiveInfo(@PathParam("id") String id) {
        CollectiveInfo info = new CollectiveInfo();
        info.setId(Identifier.collective(id));
        info.setDeliveryPolicy(DeliveryPolicy.Collective.TO_ALL_MEMBERS);
        info.setPeers(Arrays.asList(Identifier.peer("1"), Identifier.peer("2")));
        return info;
    }

    @GET
    @Path("/peerInfo/{id}")
    public PeerInfo getPeerInfo(@PathParam("id") String id) {
        PeerInfo info = new PeerInfo();
        info.setId(Identifier.peer(id));
        info.setDeliveryPolicy(DeliveryPolicy.Peer.AT_LEAST_ONE);
        info.setPrivacyPolicies(null);
        info.setAddresses(Collections.<PeerChannelAddress>emptyList());
        return info;
    }

    @GET
    @Path("/peerAuth/{id}")
    public boolean authenticatePeer(@PathParam("id") String id, @HeaderParam("password") String password) {
        return id.equals(password);
    }
}
