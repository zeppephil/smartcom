package at.ac.tuwien.dsg.smartcom.broker.policy;

/**
 * Defines the result type for the replication policy.
 *
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 * @see ReplicationPolicyResult
 */
public enum ReplicationType {
    UPSCALE, DOWNSCALE, NOSCALE;
}
