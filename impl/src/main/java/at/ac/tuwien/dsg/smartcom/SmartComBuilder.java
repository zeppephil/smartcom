package at.ac.tuwien.dsg.smartcom;

import at.ac.tuwien.dsg.smartcom.callback.CollectiveInfoCallback;
import at.ac.tuwien.dsg.smartcom.callback.PeerAuthenticationCallback;
import at.ac.tuwien.dsg.smartcom.callback.PeerInfoCallback;
import at.ac.tuwien.dsg.smartcom.exception.CommunicationException;
import at.ac.tuwien.dsg.smartcom.exception.ErrorCode;
import at.ac.tuwien.dsg.smartcom.model.MessageLogLevel;
import at.ac.tuwien.dsg.smartcom.utils.MongoDBInstance;
import com.mongodb.MongoClient;

import java.io.IOException;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
public class SmartComBuilder {
    private final SmartComConfiguration configuration;

    public SmartComBuilder(PeerAuthenticationCallback peerManager, PeerInfoCallback peerInfoCallback, CollectiveInfoCallback collectiveInfoCallback) {
        this.configuration = new SmartComConfiguration();
        this.configuration.peerManager = peerManager;
        this.configuration.peerInfoCallback = peerInfoCallback;
        this.configuration.collectiveInfoCallback = collectiveInfoCallback;
    }

    public SmartComBuilder initializeActiveMQ(boolean initActiveMQ) {
        this.configuration.initActiveMQ = initActiveMQ;
        return this;
    }

    public SmartComBuilder setActiveMqPort(int port) {
        this.configuration.activeMQPort = port;
        return this;
    }

    public SmartComBuilder setActiveMqHost(String activeMqHost) {
        this.configuration.activeMqHost = activeMqHost;
        return this;
    }

    public SmartComBuilder setRestApiPort(int port) {
        this.configuration.restAPIPort = port;
        return this;
    }

    public SmartComBuilder setMongoClient(MongoClient client) {
        this.configuration.mongoClient = client;
        return this;
    }

    public SmartComBuilder setMongoDBInstance(MongoDBInstance instance) {
        this.configuration.mongoDB = instance;
        return this;
    }

    public SmartComBuilder initAdapters(boolean initAdapters) {
        this.configuration.initAdapters = initAdapters;
        return this;
    }

    public SmartComBuilder useLocalMessageQueue(boolean useLocalMQ) {
        this.configuration.useLocalMQ = useLocalMQ;
        return this;
    }

    public SmartComBuilder setMongoDBDatabaseName(String databaseName) {
        this.configuration.mongoDBDatabaseName = databaseName;
        return this;
    }

    public SmartComBuilder setMessageLogLevel(MessageLogLevel level) {
        this.configuration.messageLogLevel = level;
        return this;
    }

    public SmartComBuilder setMessageInfoServicePort(int port) {
        this.configuration.misAPIPort = port;
        return this;
    }

    public SmartCom create() throws CommunicationException {
        if (this.configuration.mongoClient == null) {
            try {
                if (this.configuration.mongoDB == null) {
                    this.configuration.mongoDB = new MongoDBInstance(-1); //uses standard port
                    this.configuration.mongoDB.setUp();
                }

                this.configuration.mongoClient = this.configuration.mongoDB.getClient();
            } catch (IOException e) {
                throw new CommunicationException(e, new ErrorCode(1, "Could not create mongo database"));
            }
        }

        SmartCom smartCom = new SmartCom(configuration);
        smartCom.initializeSmartCom();

        return smartCom;
    }
}
