package at.ac.tuwien.dsg.smartcom;

import at.ac.tuwien.dsg.smartcom.callback.CollectiveInfoCallback;
import at.ac.tuwien.dsg.smartcom.callback.PeerAuthenticationCallback;
import at.ac.tuwien.dsg.smartcom.callback.PeerInfoCallback;
import at.ac.tuwien.dsg.smartcom.model.MessageLogLevel;
import at.ac.tuwien.dsg.smartcom.utils.MongoDBInstance;
import com.mongodb.MongoClient;

/**
* @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
* @version 1.0
*/
public class SmartComConfiguration {
    public static final String MONGODB_DATABASE = "SmartCom";
    public static final int ACTIVE_MQ_DEFAULT_PORT = 61616;
    public static final String ACTIVE_MQ_DEFAULT_HOST = "localhost";
    public static final int REST_API_DEFAULT_PORT = 8080;
    public static final int MIS_API_DEFAULT_PORT = 8081;
    public static final boolean ADAPTER_INITIALISATION_DEFAULT = true;
    public static final MessageLogLevel DEFAULT_MESSAGE_LOGLEVEL = MessageLogLevel.NONE;

    //Dependencies configuration
    PeerAuthenticationCallback peerManager;
    PeerInfoCallback peerInfoCallback;
    CollectiveInfoCallback collectiveInfoCallback;

    //MongoDB configuration
    MongoClient mongoClient;
    MongoDBInstance mongoDB;
    String mongoDBDatabaseName = MONGODB_DATABASE;

    //ActiveMQ configuration
    boolean useLocalMQ = false;
    boolean initActiveMQ = true;
    String activeMqHost = ACTIVE_MQ_DEFAULT_HOST;
    int activeMQPort = ACTIVE_MQ_DEFAULT_PORT;
    MessageLogLevel messageLogLevel = DEFAULT_MESSAGE_LOGLEVEL;

    int restAPIPort = REST_API_DEFAULT_PORT;
    boolean initAdapters = ADAPTER_INITIALISATION_DEFAULT;

    int misAPIPort = MIS_API_DEFAULT_PORT;
}
