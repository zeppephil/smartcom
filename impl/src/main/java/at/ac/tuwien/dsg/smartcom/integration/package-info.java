/**
 * The purpose of this package is to integrate with other components of the SmartSociety platform.
 * Currently we implemented a connector to the Peer Manager (class SmartSocRESTPeerManager) that is compatible
 * with the API level v0.65 of the Peer Manager.
 * <br/><br/>
 * The base urls for the requests of the Peer Manager can be changed by creating a file "pm.properties" and setting
 * the following properties:
 * <ul>
 *  <li>collectiveURL</li>
 *  <li>peerURL</li>
 *  <li>authenticationURL</li>
 *</ul>
 * @see at.ac.tuwien.dsg.smartcom.integration.SmartSocRESTPeerManager
 */
package at.ac.tuwien.dsg.smartcom.integration;