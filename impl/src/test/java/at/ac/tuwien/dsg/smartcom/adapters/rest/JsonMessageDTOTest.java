package at.ac.tuwien.dsg.smartcom.adapters.rest;

import at.ac.tuwien.dsg.smartcom.model.Identifier;
import at.ac.tuwien.dsg.smartcom.model.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JsonMessageDTOTest {

    @Test
    public void testCreateMessage_allFields() throws Exception {
        final Message message = new Message.MessageBuilder()
                .setId(Identifier.message("testId"))
                .setContent("testContent")
                .setType("testType")
                .setSubtype("testSubType")
                .setSenderId(Identifier.peer("sender"))
                .setReceiverId(Identifier.peer("receiver"))
                .setConversationId("conversationId")
                .setTtl(3)
                .setLanguage("testLanguage")
                .setSecurityToken("securityToken")
                .create();

        JsonMessageDTO dto = new JsonMessageDTO(message);
        assertEquals(message, dto.createMessage());
    }

    @Test
    public void testCreateMessage_nullId() throws Exception {
        final Message message = new Message.MessageBuilder()
                .setContent("testContent")
                .setType("testType")
                .setSubtype("testSubType")
                .setSenderId(Identifier.peer("sender"))
                .setReceiverId(Identifier.peer("receiver"))
                .setConversationId("conversationId")
                .setTtl(3)
                .setLanguage("testLanguage")
                .setSecurityToken("securityToken")
                .create();

        JsonMessageDTO dto = new JsonMessageDTO(message);
        assertEquals(message, dto.createMessage());
    }

    @Test
    public void testCreateMessage_nullSender() throws Exception {
        final Message message = new Message.MessageBuilder()
                .setId(Identifier.message("testId"))
                .setContent("testContent")
                .setType("testType")
                .setSubtype("testSubType")
                .setReceiverId(Identifier.peer("receiver"))
                .setConversationId("conversationId")
                .setTtl(3)
                .setLanguage("testLanguage")
                .setSecurityToken("securityToken")
                .create();

        JsonMessageDTO dto = new JsonMessageDTO(message);
        assertEquals(message, dto.createMessage());
    }

    @Test
    public void testCreateMessage_nullReceiver() throws Exception {
        final Message message = new Message.MessageBuilder()
                .setId(Identifier.message("testId"))
                .setContent("testContent")
                .setType("testType")
                .setSubtype("testSubType")
                .setSenderId(Identifier.peer("sender"))
                .setConversationId("conversationId")
                .setTtl(3)
                .setLanguage("testLanguage")
                .setSecurityToken("securityToken")
                .create();

        JsonMessageDTO dto = new JsonMessageDTO(message);
        assertEquals(message, dto.createMessage());
    }
}