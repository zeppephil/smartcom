package at.ac.tuwien.dsg.smartcom.integration;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
* Created by Philipp on 11.11.2014.
*/
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PeerManagerResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("collective/{id}")
    public String getCollectiveInfo(@PathParam("id") String id) {
        String body =
            "{" +
                "\"name\": [\"test\", \"test1\", \"test3\"]," +
                "\"owner\": {}," +
                "\"members\": [" +
                    "{ " +
                        "\"username\": \"test1\"," +
                        "\"password\": \"test1\"," +
                        "\"peerId\": 1," +
                        "\"mainProfileDefinitionId\": 2," +
                        "\"defaultPolicies\": []," +
                        "\"profileDefinitions\": []," +
                        "\"id\": 1" +
                    "}," +
                    "{ " +
                        "\"username\": \"test2\"," +
                        "\"password\": \"test2\"," +
                        "\"peerId\": 2," +
                        "\"mainProfileDefinitionId\": 3," +
                        "\"defaultPolicies\": []," +
                        "\"profileDefinitions\": []," +
                        "\"id\": 2" +
                    "}," +
                "]," +
                "\"id\": 1" +
            "}";
        return body;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("peer/{id}")
    public String getPeerInfo(@PathParam("id") String id) {
        String body =
                "{" +
                    "\"@type\": \"EQLSearchResult\"," +
                    "\"results\": ["+
                        "["+
                            "\"1\","+
                            "\"2\""+
                        "]"+
                    "]"+
                "}";
        return body;
    }

}
