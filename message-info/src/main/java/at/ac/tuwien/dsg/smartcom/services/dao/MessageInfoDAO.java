package at.ac.tuwien.dsg.smartcom.services.dao;

import at.ac.tuwien.dsg.smartcom.model.MessageInformation;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
public interface MessageInfoDAO {

    /**
     * Insert new message information.
     * @param information the message information
     */
    void insert(MessageInformation information);

    /**
     * Find a message information identified by a message as the key.
     *
     * It will return either the corresponding message information or null if there is no such
     * message information available.
     *
     * @param key key of the message information
     * @return the message information or null if there is no such message information
     */
    MessageInformation find(MessageInformation.Key key);
}
