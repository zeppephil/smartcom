package at.ac.tuwien.dsg.smartcom.services.rest;

import at.ac.tuwien.dsg.smartcom.exception.UnknownMessageException;
import at.ac.tuwien.dsg.smartcom.model.Message;
import at.ac.tuwien.dsg.smartcom.model.MessageInformation;
import at.ac.tuwien.dsg.smartcom.services.MessageInfoService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageInfoResource {

    @Inject
    private MessageInfoService mis;

    @GET
    public MessageInformation getMessageInformation(@QueryParam("type") String type,
                                                    @QueryParam("subtype") String subtype) {

        Message message = new Message.MessageBuilder().setType(type).setSubtype(subtype).create();

        try {
            return mis.getInfoForMessage(message);
        } catch (UnknownMessageException e) {
            throw new WebApplicationException(
                    "There is no message info for that query!",
                    Response.Status.NOT_FOUND.getStatusCode());
        }
    }

    @POST
    public Response postMessageInformation(MessageInformation info) {
        Message message = new Message.MessageBuilder()
                .setType(info.getKey().getType())
                .setSubtype(info.getKey().getSubtype())
                .create();
        mis.addMessageInfo(message, info);

        return Response.status(Response.Status.OK).build();
    }
}
