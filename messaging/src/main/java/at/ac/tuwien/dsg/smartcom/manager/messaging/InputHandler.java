package at.ac.tuwien.dsg.smartcom.manager.messaging;

import at.ac.tuwien.dsg.smartcom.broker.*;
import at.ac.tuwien.dsg.smartcom.broker.policy.DynamicReplicationPolicy;
import at.ac.tuwien.dsg.smartcom.model.Message;
import at.ac.tuwien.dsg.smartcom.statistic.StatisticBean;
import at.ac.tuwien.dsg.smartcom.utils.PredefinedMessageHelper;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
public class InputHandler implements MessageListener{

    private final MessagingAndRoutingManagerImpl manager;
    private final MessageBroker broker;
    private final StatisticBean statistic;

    private ReplicatingMessageListener inputListener;
    private ReplicatingMessageListener controlListener;
    private CancelableListener cancelableInputListener;
    private CancelableListener cancelableControlListener;

    public InputHandler(MessagingAndRoutingManagerImpl manager, MessageBroker broker, StatisticBean statistic) {
        this.manager = manager;
        this.broker = broker;
        this.statistic = statistic;
    }

    public void init() {
        inputListener = new ReplicatingMessageListener("input", this, new ReplicationFactory() {
            @Override
            public MessageListener createReplication() {
                return InputHandler.this;
            }
        }, new DynamicReplicationPolicy());
        cancelableInputListener = broker.registerInputListener(inputListener);

        controlListener = new ReplicatingMessageListener("control", this, new ReplicationFactory() {
            @Override
            public MessageListener createReplication() {
                return InputHandler.this;
            }
        }, new DynamicReplicationPolicy());
        cancelableControlListener = broker.registerControlListener(controlListener);
    }

    public void destroy() {
        cancelableInputListener.cancel();
        cancelableControlListener.cancel();
        inputListener.shutdown();
        controlListener.shutdown();
    }


    @Override
    public void onMessage(Message message) {
        if (PredefinedMessageHelper.CONTROL_TYPE.equals(message.getType())) {
            statistic.controlReceived();
        } else {
            statistic.inputReceived();
        }
        manager.send(message);
    }
}
