package at.ac.tuwien.dsg.smartcom.manager.messaging.util;

/**
 * @author Philipp Zeppezauer (philipp.zeppezauer@gmail.com)
 * @version 1.0
 */
public final class KeyProvider {
    private KeyProvider() {
    }

    /**
     * Generate an unique id
     * @return unique id
     */
    public static String generateUniqueIdString() {
        return TimeBasedUUID.getUUIDAsString();
    }
}
