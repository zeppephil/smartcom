package at.ac.tuwien.dsg.smartcom.manager.messaging.policies.privacy.peer;

import at.ac.tuwien.dsg.smartcom.model.Message;

public class AlwaysFailsDummyPeerPrivacyPolicy extends AbstractPrivacyPolicy {
	
	
	public AlwaysFailsDummyPeerPrivacyPolicy(){
		super("AlwaysFailsDummyPeerPrivacyPolicy");
	}
	
	@Override
	public boolean condition(Message msg){
		return false;
	}
	
}
